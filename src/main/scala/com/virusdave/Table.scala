package com.virusdave

/**
  * Hypothetical table schema.
  *
  * Turns out it isn't even needed for the "nested case classes" example.
  */

object Tables extends {
  val profile = slick.driver.PostgresDriver
} with Tables

trait Tables {
  val profile: slick.driver.JdbcProfile

  import profile.api._
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Array(Tabula.schema).reduceLeft(_ ++ _)
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  case class TabulaRow(int: Int, long: Long, string: String, boolean: Boolean)
  /** GetResult implicit for fetching ComOdoformsFoldersRow objects using plain SQL queries */
  implicit def GetResultTabulaRow(implicit e0: GR[Int], e1: GR[Long], e2: GR[String], e3: GR[Boolean]): GR[TabulaRow] = GR{
    prs => import prs._
    TabulaRow.tupled((<<[Int], <<[Long], <<[String], <<[Boolean]))
  }
  /** Table description of table com_odoforms_folders. Objects of this class serve as prototypes for rows in queries. */
  class Tabula(_tableTag: Tag) extends Table[TabulaRow](_tableTag, "tablula") {
    def * = (int, long, string, boolean) <> (TabulaRow.tupled, TabulaRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(int), Rep.Some(long), Rep.Some(string), Rep.Some(boolean)).shaped.<>({r=>import r._; _1.map(_=> TabulaRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    val int: Rep[Int] = column[Int]("int")
    val long: Rep[Long] = column[Long]("long")
    val string: Rep[String] = column[String]("string", O.Length(2147483647,varying=true))
    val boolean: Rep[Boolean] = column[Boolean]("boolean")
  }
  /** Collection-like TableQuery object for table ComOdoformsFolders */
  lazy val Tabula = new TableQuery(tag => new Tabula(tag))
}

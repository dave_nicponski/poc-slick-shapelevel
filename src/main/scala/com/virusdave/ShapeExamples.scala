package com.virusdave

import slick.driver.PostgresDriver.api._

object ShapeExamples {
  case class Left(int: Int, long: Long)
  case class LeftLift(int: Rep[Int], long: Rep[Long])
  implicit object LeftShape extends CaseClassShape(LeftLift.tupled, Left.tupled)

  case class Right(string: String, boolean: Boolean)
  case class RightLift(string: Rep[String], boolean: Rep[Boolean])
  implicit object RightShape extends CaseClassShape(RightLift.tupled, Right.tupled)

  case class Nested(left: Left, right: Right)
  case class NestedLift(left: Rep[Left], right: Rep[Right])
  case class NestedLift2(left: LeftLift, right: RightLift)

  /*
   The below fails to compile:
      [error] Slick does not know how to map the given types.
      [error] Possible causes: T in Table[T] does not match your * projection. Or you use an unsupported type in a Query (e.g. scala List).
      [error]   Required level: slick.lifted.FlatShapeLevel
      [error]      Source type: (slick.driver.PostgresDriver.api.Rep[com.virusdave.ShapeExamples.Left], slick.driver.PostgresDriver.api.Rep[com.virusdave.ShapeExamples.Right])
      [error]    Unpacked type: (com.virusdave.ShapeExamples.Left, com.virusdave.ShapeExamples.Right)
      [error]      Packed type: (slick.driver.PostgresDriver.api.Rep[com.virusdave.ShapeExamples.Left], slick.driver.PostgresDriver.api.Rep[com.virusdave.ShapeExamples.Right])
      [error]   implicit object NestedShape extends CaseClassShape(NestedLift.tupled, Nested.tupled)
      [error]                                       ^
      [error] one error found
    */
  //implicit object NestedShape extends CaseClassShape(NestedLift.tupled, Nested.tupled)
  implicit object NestedShape extends CaseClassShape(NestedLift2.tupled, Nested.tupled)
}